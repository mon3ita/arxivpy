import unittest
import os

from arxivpy.client import ArXivPyClient

class TestArxivPy(unittest.TestCase):

  def setUp(self):
    self.client = ArXivPyClient()
    self.dir_path = os.path.dirname(os.path.realpath(__file__)) + '/results'
    #make dir...

  def test_simple_search(self):
    result = self.client.search.simple(query='quantum')
    self.assertTrue(len(result) > 0)

  def test_advanced_search_with_simple_term(self):
    result = self.client.search.advanced(term='quantum')
    self.assertTrue(len(result) > 0)

  def test_advanced_search_with_dict_term(self):
    result = self.client.search.advanced(term={'term': 'quantum',
                                                'operator': 'AND',
                                                'field': 'title'})
    self.assertTrue(len(result) > 0)

  def test_advanced_search_with_complex_term(self):
    result = self.client.search.advanced(term='quantum computation and information')
    self.assertTrue(len(result) > 0)

  def test_advanced_search_with_multiple_terms(self):
    result = self.client.search.advanced(terms=[{'term': 'quantum',
                                                'operator': 'AND',
                                                'field': 'title'},
                                                {'term': 'computing',
                                                'operator': 'OR',
                                                'field': 'title'},
                                                ])
    self.assertTrue(len(result) > 0)

  def test_with_physics(self):
    result = self.client.search.advanced(term='quantum', 
                                          classification='physics, computer_science', 
                                          physics='quant-ph')
    print("{}\n{}".format(result[0].title, result[0].authors))
    self.assertTrue(len(result) > 0)